from random import randint

player_name = input("What is your name? ")

# month_number = randint(1, 12)
# month_year = randint(1924, 2002)

# random_guess = [1, 2, 3, 4, 5]


# for guess in random_guess:
#     print(f"Guess {guess} : {player_name} were you born on {randint(1, 12)} / {randint(1924, 2002)}?")
#     response = input("Yes or No? ").lower()
#     if response == "yes":
#         print("I knew it")
#         exit()
#     else:
#         print("Drats! Lemme try again!")


for guess in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print(f"Guess {guess} : were you born in {month_number} / {year_number}")
    response = input("Yes or No?").lower()

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess == 5:
        print("I have other things to do. Goodbye.")
    else:
        print("Drats! Lemme try again!")
