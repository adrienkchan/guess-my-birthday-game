from random import randint

player_name = input("What is your name? ")

guess_number = 1
month_number = randint(1, 12)
year_number = randint(1924, 2004)

print(
    f"Guess {guess_number} : {player_name} were you born on {month_number} / {year_number}?"
)

response = input("Is this answer correct? Yes or No?").lower()

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

guess_number = 2
month_number = randint(1, 12)
year_number = randint(1924, 2004)

print(
    f"Guess {guess_number} : {player_name} were you born on {month_number} / {year_number}?"
)

response = input("Is this answer correct? Yes or No?").lower()

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

guess_number = 3
month_number = randint(1, 12)
year_number = randint(1924, 2004)

print(
    f"Guess {guess_number} : {player_name} were you born on {month_number} / {year_number}?"
)

response = input("Is this answer correct? Yes or No?").lower()

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

guess_number = 4
month_number = randint(1, 12)
year_number = randint(1924, 2004)

print(
    f"Guess {guess_number} : {player_name} were you born on {month_number} / {year_number}?"
)

response = input("Is this answer correct? Yes or No?").lower()

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

guess_number = 5
month_number = randint(1, 12)
year_number = randint(1924, 2004)

print(
    f"Guess {guess_number} : {player_name} were you born on {month_number} / {year_number}?"
)

response = input("Is this answer correct? Yes or No?").lower()

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("I have other things to do. Good bye.")
